/*
db_navigator.js  - java script functions for navigator tab of openmat-webui

Copyright 2020 Thies Hecker

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

$( function () {
  
  update_tree();  
   
  // launches the 'get_tree' plug-in
  function update_tree () 
  { 
 		var plugin_data = {
			name: 'get_tree',
			params: []
		};
    
    console.log(plugin_data);    
    
    var response = $.ajax({
			type: "POST",
			url: "http://127.0.0.1:5010/run_plugin",
			data: JSON.stringify(plugin_data),
			dataType: 'json',
			crossDomain: true,
			success: convert_tree
		});
  }
  
  // converts the response to format compatible with the fancytree widget
  function convert_tree (response) 
  {
    var tree_list = [];
    // the response is a dictionary with keys for each collection and each value is a list of record names
    Object.keys(response).forEach(function(key) {
      console.log(key, response[key]);
      var collection_dict = {title: key, folder: true, children: []}
      response[key].forEach(function (item, index) {
        collection_dict.children.push({title: item});
      });
      tree_list.push(collection_dict);
    });
    
    console.log(tree_list);
    
    // create the tree view widget
    $("#db_tree").fancytree({
      source: tree_list,
      activate: get_record
    });
    
  }

  function get_record(event, data){
    active_node = data.node;
    active_collection = active_node.parent;

    if (active_collection.title != 'root'){
      $("#active_record").text(active_node.title);
      $("#active_collection").text(active_collection.title);
      fetch_record(active_collection.title, active_node.title);
    }
    else {
      $("#active_collection").text(active_node.title);
    }
  }
  
  function fetch_record (collection, record_id) {
    var plugin_data = {
			name: 'record_by_ID',
			params: [collection, record_id]
		};
    
    console.log(plugin_data);    
    
    var response = $.ajax({
			type: "POST",
			url: "http://127.0.0.1:5010/run_plugin",
			data: JSON.stringify(plugin_data),
			dataType: 'json',
			crossDomain: true,
			success: display_record_data
		});
  }
  
  function display_record_data (response) {
    $("#record_data").text(JSON.stringify(response, null, 2));
  }
  
});