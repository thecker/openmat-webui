/*
plugins.js  - java script functions for plug-ins tab of openmat-webui

Copyright 2020 Thies Hecker

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


$ ( function() {
	// -----------------------
   // initialize UI elements
   // -----------------------

	$("#ButRun")
		.button()
		.click(run_plugin);
   
   $( "#SelectPlugin" )
    	.selectmenu({
    		create: fetch_plugins,
    		select: display_plugin_desc
   });
 
    
   // --------------------------
   // python interface functions
   // --------------------------
    
   // fetches a list of available plugins displays it	
   function fetch_plugins () 
	{
		$.ajax({
			type: "GET",
			url: 	"http://127.0.0.1:5010/show_plugins",
			data: null,
			dataType: 'json',
			crossDomain: true,
			success: display_plugins
		});
	};
   
   // runs the selected plugin and displays results
   function run_plugin (){
		//read plugin name and parameter values
		//TODO: Make sure $.plugin_no is initialized...
		plugin_name = $.plugin_infos[$.plugin_no]['module'];
		params = read_ui_values();
		
		var plugin_data = {
			name: plugin_name,
			params: params
		}
		
		//send call to python-interface		
		$.ajax({
			type: "POST",
			url: "http://127.0.0.1:5010/run_plugin",
			data: JSON.stringify(plugin_data),
			dataType: 'json',
			crossDomain: true,
			success: display_results
		});
   }

	// ---------------------------
	// UI element update functions
	// ---------------------------
	
	// saves plugin infos returned from python interface to $.pluginfo and updates plugin-select
   function display_plugins (response) {
   	$.plugin_infos = response;
		console.log(response);
		var options = []; 
    	for (i = 0; i < response.length; i++) {
        options.push("<option value='" + response[i]['name'] + "'>" + response[i]['name'] + "</option>");
    	}
    	console.log(options);
    	//append after populating all options
    	$('#SelectPlugin')
        .append(options.join(""))
        .selectmenu();
   };
   
   // displays the plugin's help text based on the 
   function display_plugin_desc (ev, ui){
		$.plugin_no = ui.item.index;   	
   	var desc = $.plugin_infos[$.plugin_no]['help'];
   	if (desc == null){
      desc = 'No help available for this plugin.';   	
   	}
   	console.log(ui);
   	console.log(desc);
		$( "#PluginDesc" ).text(desc);
		add_plugin_UI_elements($.plugin_no);
   }
   
   // adds plugin UI elements to main area
   function add_plugin_UI_elements (plugin_no){
   	ui_elements = $.plugin_infos[plugin_no]['UI_elements'];
   	
   	// remove all UI elements
   	$( "#Plugin_UI_elements" ).empty();
   	
   	// update UI elements
   	$.ui_names = []	// this holds the currently available ui element names
		for (const [index, ui_element] of ui_elements.entries()){
			console.log(ui_element);
			label = ui_element['label'];
			default_value = ui_element['default_value'];
			
			// check if active collection or record should be used as default value 
			if (default_value == ''){
			  switch(label) {
			    case 'Collection':
			      if ($("#active_collection").text() != null){
              default_value = $("#active_collection").text();
              break;    
            }
          case 'Record ID':
            if ($("#active_record").text() != null){
              default_value = $("#active_record").text();
              break;          
            }
          case 'UI type':
            default_value = 'browser';
            break;
			  }	
			}
			ui_name = "Input" + index;
			$.ui_names.push(ui_name);
			html_str = label + ": <input id='" + ui_name + "' value='" + default_value + "' type='text'><br>";
			console.log(html_str);
			$( "#Plugin_UI_elements" ).append(html_str);	 
		}
   }
   
   // reads parameter values from UI elements
   function read_ui_values(){
   	var param_values = [];
		for (const ui_name of $.ui_names){
			console.log('reading ' + ui_name);
			param_values.push($( "#" + ui_name ).val());
		}
		console.log(param_values);
		return param_values;
   }
   
   //displays the results of a plugin execution
   function display_results(response) {
		console.log(response);
		plugin_return_type = $.plugin_infos[$.plugin_no]['return_type'];
		console.log(plugin_return_type);
		switch(plugin_return_type) {
			case 'list':
				result_text = ''
				for (element of response){
					result_text += JSON.stringify(element, null, 2);				
				}
				$("#Plugin_Results").text(result_text);
				break;
			case 'text':
				$("#Plugin_Results").text(JSON.stringify(response, null, 2));
				break;
			case 'pyplot':
			  $("#Plugin_Results").text(''); // clear previous results
			  $("#Plugin_Results").append(response); //.replace('\n',''));
			  console.log('pyplot return type');
			  break;
			default:
				$("#Plugin_Results").text(JSON.stringify(response, null, 2));
				break;
   	}
   }

});
