About
=====

*openmat-webui* is a the web browser-based GUI for *openmatDB*.

*openmat-webui* is free software released itself under the Apache License Version 2.0.

It comes bundle with following software components:

* jQuery JavaScript Library v1.12.4, Copyright jQuery Foundation and other contributors
* jQuery UI v1.12.1, Copyright jQuery Foundation and other contributors
* jQuery UI-themes v1.12.1, Copyright jQuery Foundation and other contributors
* Fancytree, v2.34.0, Copyright 2008-2019 Martin Wendt

See the NOTICE and LICENSE file for details.

*openmat-webui* is written python and JavaScript. The JavaScript dependencies are bundled with the software
(see above). For the python part following packages are required:

* python3
* Flask
* Flask-CORS
* pyopenmatdb

For more information on this project and *openmatDB* see: https://openmatdb.readthedocs.io/en/latest/
