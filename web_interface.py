# web_interface.py - web server for the openmat-webui
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import Flask, request, send_from_directory
from flask_cors import CORS
import json
from openmatdb.pyopenmatdb import OpenmatEnvironment


class PythonInterface(Flask):

    def __init__(self, __name__):

        super().__init__(__name__)
        self.openmat_env = None


app = PythonInterface(__name__)
CORS(app)


@app.route("/", methods=['GET', 'POST'])
def show_webpage():
    """This returns the html page"""
    return send_from_directory('webpage', 'index.html')


@app.route("/plugins.html",  methods=['GET', 'POST'])
def show_plugins_html():
    """Returns the plug-ins page"""
    return send_from_directory('webpage', 'plugins.html')


@app.route("/db_navigator.html",  methods=['GET', 'POST'])
def show_db_navigator_html():
    """Returns the plug-ins page"""
    return send_from_directory('webpage', 'db_navigator.html')

# @app.route("/results.html",  methods=['GET', 'POST'])
# def show_results_html():
#     """Returns the results page"""
#     return send_from_directory('webpage', 'results.html')


@app.route('/static/jquery-ui-themes/themes/base/images/<path:path>')
def send_images(path):
    """This is used for the jquery UI icons"""
    return send_from_directory('webpage/static/jquery-ui-themes/themes/base/images', path)

# TODO: serving static files can probably be automized (i.e. not each file served specifically...)
# below functions serve the static content (javascript and css)
@app.route("/static/main.js")
def send_js():
    return send_from_directory('webpage/static', 'main.js')


@app.route("/static/plugins.js")
def send_plugins_js():
    return send_from_directory('webpage/static', 'plugins.js')


@app.route("/static/db_navigator.js")
def send_navigator_js():
    return send_from_directory('webpage/static', 'db_navigator.js')


@app.route("/static/jquery-ui/external/jquery/jquery.js")
def send_jquery():
    return send_from_directory('webpage/static/jquery-ui/external/jquery', 'jquery.js')


@app.route("/static/jquery-ui/jquery-ui.js")
def send_jquery_ui():
    return send_from_directory('webpage/static/jquery-ui', 'jquery-ui.js')


@app.route("/node_modules/jquery.fancytree/dist/jquery.fancytree-all.min.js")
def send_fancytree_js():
    return send_from_directory('webpage/node_modules/jquery.fancytree/dist',
                               'jquery.fancytree-all.min.js')


@app.route("/node_modules/jquery.fancytree/dist/skin-awesome/ui.fancytree.css")
def send_fancytree_css():
    return send_from_directory('webpage/node_modules/jquery.fancytree/dist/skin-awesome',
                               'ui.fancytree.css')

# serve jQuery-ui css files
@app.route("/static/jquery-ui-themes/themes/base/jquery-ui.css")
def send_css():
    return send_from_directory('webpage/static/jquery-ui-themes/themes/base', 'jquery-ui.css')

# serve openmatDB webui main window css files
@app.route("/static/css/main.css")
def send_main_css():
    return send_from_directory('webpage/static/css', 'main.css')

# serve openmatDB webui plugins-tab css files
@app.route("/static/css/plugins.css")
def send_plugins_css():
    return send_from_directory('webpage/static/css', 'plugins.css')


@app.route("/static/css/db_navigator.css")
def send_db_navigator_css():
    return send_from_directory('webpage/static/css', 'db_navigator.css')


# these are the back-end functions
@app.route("/show_plugins", methods=['GET'])
def show_plugins():
    """Returns the available plugins"""
    plugin_config = app.openmat_env.plugin_config
    return json.dumps(plugin_config)


@app.route("/run_plugin", methods=['POST'])
def run_plugin():
    """Runs a plugin and returns results

    Note:
        Requester must send a dictionary with keys 'name' and 'params' (list of parameter values)
    """
    data_str = request.get_data(as_text=True)
    data = json.loads(data_str)
    plugin_name = data['name']  # name of plugin
    params = data['params']     # list of plugin param values

    result = app.openmat_env.run_plugin(plugin_name, *params)

    return json.dumps(result, default=str)


if __name__ == "__main__":

    #TODO: This is the URL if the openmatDB web UI runs in a docker container, if run from localhost it will be 0.0.0.0:5000/v1
    openmat_env = OpenmatEnvironment()

    app.openmat_env = openmat_env

    # this is set up for the configuration of the docker image
    with open('../config/config.json') as file:
        config = json.loads(file.read())

    for plugin_server in config['plugin_server_urls']:
        openmat_env.add_plugin_server(plugin_server)

    app.run(config['address'], config['port'])
